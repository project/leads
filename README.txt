Drupal Leads module:
------------------------
Maintainers:
  Zak Huber (http://drupal.org/user/1437276)
Requires - Drupal 7
License - GPL (see LICENSE)


Overview:
--------
This module provides a very basic leads management system for managing client leads.

Leads listing page:
-------------
Go to "/leads" and you can see a list of recently created leads.